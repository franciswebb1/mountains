### Hill walking weather recommendations ###

Currently using Python 3.6, to get started, simply run main.py. 

`DAY_OFFSET` within `main.py` will determine 
how far in the future you would like to look - the default value is 0 for today, 
1 would be tomorrow etc.