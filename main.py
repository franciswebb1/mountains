import pandas as pd
import requests
from bs4 import BeautifulSoup

# Backlog
#
# 0. Sort out a better way for scoring what is determined as 'good weather'. This could also incorporate height as a
#    factor - we consider the weather at the top of the mountain - the higher hills will normally have worse conditions
#    so currently we are biased towards little lumps. Would be nice to offset this somehow...
# 1. Height filter (e.g. only return hills over or below some threshold)
# 2. Walkhighlands - currently returning the weather forecast link, would be nice to link straight to a guide
# 3. Distance - use google maps API to only return hills that are a walkable distance


FILENAME = 'forecast_saturday.csv'
DAY_OFFSET = 0


def connect_bs(url):
    try:
        r = requests.get(url)
        parsed_html = BeautifulSoup(r.text)
    except:
        parsed_html = "Connection error - 111"
    return parsed_html


class Forecast:
    def __init__(self, day_offset=0):
        self.mountain_forecasts = {}
        self.day_offset = day_offset
        self.drop_cols = ['url', 'am_forecast', 'pm_forecast']
        self.low_cols = ['am_rain', 'pm_rain', 'pm_wind', 'am_wind']

    def get_nicest_weather(self, df):
        # We want LOW wind, LOW rain, HIGH temp
        num_df = df.drop(self.drop_cols, axis=1)
        for col in num_df.columns:
            num_df[col] = num_df[col].astype(int)
        num_df['height'] = num_df['height'] * 2  # Extra weighting for high mountains
        normalized_df = (num_df - num_df.min()) / (num_df.max() - num_df.min())
        for col in self.low_cols:
            normalized_df[col] = - normalized_df[col]  # Penalise large values for rain, wind, etc
        return normalized_df.sum(axis=1)

    def parse_table_row(self, class_name, html):
        row_html = html.find('tr', {'class': class_name})
        day_values = row_html.find_all_next('td')
        am = day_values[self.day_offset * 3].text
        pm = day_values[self.day_offset * 3 + 1].text
        am = " ".join(am.split())
        pm = " ".join(pm.split())

        return am, pm

    def gen_forecast_for_mountain(self, name, height):
        url = 'https://www.mountain-forecast.com/peaks/' + name + '/forecasts/' + height
        html = connect_bs(url=url)
        int_cols = ['rain', 'wind', 'max_temp', 'min_temp']
        day_forecast = {'am_rain': '', 'pm_rain': '', 'am_wind': '', 'pm_wind': '', 'am_max_temp': '',
                        'pm_max_temp': '', 'am_min_temp': '', 'pm_min_temp': '', 'am_forecast': '', 'pm_forecast': '',
                        'url': url, 'height': height}
        forecast = self.parse_table_row(class_name='forecast__table-summary', html=html)
        day_forecast['am_forecast'] = forecast[0]
        day_forecast['pm_forecast'] = forecast[1]
        rain = self.parse_table_row(class_name='forecast__table-rain', html=html)
        day_forecast['am_rain'] = rain[0]
        day_forecast['pm_rain'] = rain[1]
        wind = self.parse_table_row(class_name='forecast__table-wind', html=html)
        day_forecast['am_wind'] = wind[0]
        day_forecast['pm_wind'] = wind[1]
        max_temp = self.parse_table_row(class_name='forecast__table-max-temperature', html=html)
        day_forecast['am_max_temp'] = max_temp[0]
        day_forecast['pm_max_temp'] = max_temp[1]
        min_temp = self.parse_table_row(class_name='forecast__table-min-temperature', html=html)
        day_forecast['am_min_temp'] = min_temp[0]
        day_forecast['pm_min_temp'] = min_temp[1]

        for col in int_cols:
            for time in ['am', 'pm']:
                if day_forecast[time + '_' + col] == '-':
                    day_forecast[time + '_' + col] = 0
                else:
                    day_forecast[time + '_' + col] = int(day_forecast[time + '_' + col])
        self.mountain_forecasts[name] = day_forecast

    def main(self):
        url = 'https://www.mountain-forecast.com/peaks/Ben-Lawers/forecasts/1204'
        html = connect_bs(url=url)
        mountains_html = html.find('select', {'class': 'js-nav-location-select'})
        mountains = mountains_html.find_all_next('option')
        for mountain in mountains[1:]:
            name = mountain['value'].split('/')[0]
            if len(mountain['value'].split('/')) > 1:
                height = mountain['value'].split('/')[1]
                # print(f'Getting forecast for {name}...')
                self.gen_forecast_for_mountain(name=name, height=height)

        pd.DataFrame(self.mountain_forecasts).to_csv(FILENAME, index=True)


if __name__ == '__main__':
    f = Forecast(day_offset=DAY_OFFSET)
    # f.main()

    mountain_forecasts = pd.read_csv(FILENAME)

    forecast_df = mountain_forecasts.drop('Unnamed: 0', axis=1).transpose()
    forecast_df.columns = mountain_forecasts['Unnamed: 0'].values

    weather_scores = f.get_nicest_weather(df=forecast_df)
    forecast_df['score'] = weather_scores

    nicest_looking_weather = forecast_df.sort_values('score', ascending=True)
    print('We recommend...')
    for index, row in nicest_looking_weather.tail().iterrows():
        print(index, row['url'])
    print('\nHills to avoid...')
    for index, row in nicest_looking_weather.head().iterrows():
        print(index, row['url'])
